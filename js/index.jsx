import "bootstrap/dist/css/bootstrap.min.css";
import "../css/transitions.css";
import "../css/style.css";

import React from "react";
import ReactDOM from "react-dom";
import Display from "./display";

ReactDOM.render(<Display />, document.getElementById("display"));
