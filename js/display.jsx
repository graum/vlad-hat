import React from "react";
import questions from "../questions.json";
import ReactCSSTransitionReplace from "react-css-transition-replace";

export default class Display extends React.Component {
  constructor(props) {
    super(props);
    var id = Math.floor(Math.random() * questions["questions"].length);
    this.state = {
      id: id,
      question: questions["questions"][id].replace(/(.{20})/g, "$1\n")
    };
  }

  random_question() {
    var id = Math.floor(Math.random() * questions["questions"].length);

    this.setState({
      id: id,
      question: questions["questions"][id].replace(/(.{20})/g, "$1\n")
    });
  }

  render() {
    return (
      <div>
        <br />
        <span
          style={{
            margin: "1vw",
            padding: "1vw",
            backgroundColor: "#5a90e8",
            border: "2vw solid #6f7f99",
            borderRadius: "10px",
            display: "inline-block",
            fontSize: "2.5vw",
            width: "26ch",
            height: "14ch"
          }}
        >
          <ReactCSSTransitionReplace
            transitionName="cross-fade"
            transitionEnterTimeout={500}
            transitionLeaveTimeout={500}
          >
            <p
              key={this.state.id}
              className="lead"
              style={{
                whiteSpace: "pre",
                fontFamily: "monospace",
                fontSize: "2.5vw",
                color: "white",
                width: "20ch",
                height: "10ch",
                textAlign: "left"
              }}
            >
              {this.state.question}
            </p>
          </ReactCSSTransitionReplace>
        </span>
        <div>
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => this.random_question()}
          >
            Get new discussion question!
          </button>
        </div>
      </div>
    );
  }
}
